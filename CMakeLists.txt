cmake_minimum_required(VERSION 3.22)
project(mupdf_slbtty_utils)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake")

find_package(MuPDF REQUIRED)

add_executable(pdf_extract_outline pdf_extract_outline.cpp)

target_link_libraries(pdf_extract_outline PRIVATE ${MUPDF_LIBRARIES})